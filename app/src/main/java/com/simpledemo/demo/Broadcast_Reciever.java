package com.simpledemo.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class Broadcast_Reciever  extends BroadcastReceiver{


    private String TAG="tag";
    private SharedPreferences preferences;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG,"msg recieved");
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){

            Bundle bundle=intent.getExtras();
            SmsMessage msg[]=null;
            String msgfrom=null;
            String msgBody=null;

            if(bundle != null){
                Object[] pdus = (Object[])bundle.get("pdus");
                msg = new SmsMessage[pdus.length];
                for(int i=0; i<msg.length; i++){
                    msg[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    msgfrom = msg[i].getOriginatingAddress();

                    msgBody = msg[i].getMessageBody();
                }
                Toast.makeText(context, "msg from:"+msgfrom+" message :" + msgBody, Toast.LENGTH_SHORT).show();}
            Log.v(TAG,msgBody);
        }

    }
}
