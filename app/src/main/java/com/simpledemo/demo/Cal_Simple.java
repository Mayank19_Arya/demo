package com.simpledemo.demo;

import android.app.DatePickerDialog;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Calendar;

public class Cal_Simple extends AppCompatActivity {
    ImageView imageView1,imageView2;
    EditText editTextStart,editTextEnd;
    private int mmonth, mday, myear;
    public String str;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal__simple);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editTextStart = (EditText) findViewById(R.id.editText4);
        editTextEnd = (EditText) findViewById(R.id.editText5);
        imageView1 = (ImageView) findViewById(R.id.imageView4);
        imageView2 = (ImageView) findViewById(R.id.imageView5);
        //start date listener
        imageView1.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("ResourceType")
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mmonth = c.get(Calendar.MONTH);
                mday = c.get(Calendar.DAY_OF_MONTH);
                myear = c.get(Calendar.YEAR);
                c.add(Calendar.MONTH, -1);
                long max = c.getTime().getTime();

                //  DateTime dateTime = new DateTime();
                //long milli=dateTime.minusDays(31).getMillis();
                DatePickerDialog datePickerDialog = new DatePickerDialog(Cal_Simple.this,AlertDialog.THEME_HOLO_DARK,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view1, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {

                                    str = (dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    Toast.makeText(Cal_Simple.this, "Start Date set", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    Toast.makeText(Cal_Simple.this, "Exception", Toast.LENGTH_SHORT).show();
                                }
                                editTextStart.setText(str);

                            }
                        }, myear, mmonth, mday);
                // datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis()-31*24*3600*1000l);
                //datePickerDialog.getDatePicker().setMaxDate(dateTime.minusDays(31).getMillis());
                datePickerDialog.getDatePicker().setMaxDate(max);
                datePickerDialog.setTitle("");

                datePickerDialog.show();
            }
        });
        //end date click listener
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mmonth = c.get(Calendar.MONTH);
                mday = c.get(Calendar.DAY_OF_MONTH);
                myear = c.get(Calendar.YEAR);
                //  long min=c.getTimeInMillis();
                //    c.add(Calendar.MONTH,-1);
                //long max=c.getTime().getTime();

                //  DateTime dateTime = new DateTime();
                //long milli=dateTime.minusDays(31).getMillis();
                DatePickerDialog datePickerDialog = new DatePickerDialog(Cal_Simple.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {

                                    str = (dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    Toast.makeText(Cal_Simple.this, "End Date set", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    Toast.makeText(Cal_Simple.this, "Exception", Toast.LENGTH_SHORT).show();
                                }
                                editTextEnd.setText(str);

                            }
                        }, myear, mmonth, mday);
                //datePickerDialog.getDatePicker().setMinDate(min);

                // datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis()-31*24*3600*1000l);
                //datePickerDialog.getDatePicker().setMaxDate(dateTime.minusDays(31).getMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        //end date
    }
    
    //adding Back tohome arrow
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    }



