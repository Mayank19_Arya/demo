package com.simpledemo.demo;

import android.app.DatePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.Calendar;

public class Cal_TIET extends AppCompatActivity {


    ImageView iv,iv2;
    EditText et,et2;
    private int mmonth, mday, myear;
    public String str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dynamic_cal_layout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      /*  et = (EditText) findViewById(R.id.start_date);
        et2 = (EditText) findViewById(R.id.end_date);
        iv = (ImageView) findViewById(R.id.imageView6);
        iv2 = (ImageView) findViewById(R.id.imageView7);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mmonth = c.get(Calendar.MONTH);
                mday = c.get(Calendar.DAY_OF_MONTH);
                myear = c.get(Calendar.YEAR);
                c.add(Calendar.MONTH,-1);
                long max=c.getTime().getTime();
                DatePickerDialog datePickerDialog= new DatePickerDialog(Cal_TIET.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view1, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {

                                    str = (dayOfMonth + "-" + (monthOfYear+ 1) + "-" + year);
                                    Toast.makeText(Cal_TIET.this,"Start Date set",Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                    Toast.makeText(Cal_TIET.this,"Exception",Toast.LENGTH_SHORT).show();
                                }
                                et.setText(str);

                            }
                        },myear,mmonth,mday);
                datePickerDialog.getDatePicker().setMaxDate(max);
                datePickerDialog.setTitle("");

                datePickerDialog.show();
            }
        });
        //end date
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mmonth = c.get(Calendar.MONTH);
                mday = c.get(Calendar.DAY_OF_MONTH);
                myear = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog= new DatePickerDialog(Cal_TIET.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {

                                    str = (dayOfMonth + "-" + (monthOfYear+ 1) + "-" + year);
                                    Toast.makeText(Cal_TIET.this,"End Date set",Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                    Toast.makeText(Cal_TIET.this,"Exception",Toast.LENGTH_SHORT).show();
                                }
                                et2.setText(str);

                            }
                        },myear,mmonth,mday);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
*/
        //dynamic layout
        //Text fielda
        final android.support.design.widget.TextInputEditText starttextInputEditText=new android.support.design.widget.TextInputEditText(this);
        final android.support.design.widget.TextInputEditText endtextInputEditText=new android.support.design.widget.TextInputEditText(this);
        starttextInputEditText.setHint("Start Date");
        endtextInputEditText.setHint("End Date");
        //Image View
        ImageView startimageView=new ImageView(this);
        startimageView.setImageDrawable(getResources().getDrawable(R.mipmap.cal));
        startimageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mmonth = c.get(Calendar.MONTH);
                mday = c.get(Calendar.DAY_OF_MONTH);
                myear = c.get(Calendar.YEAR);
                c.add(Calendar.MONTH,-1);
                long max=c.getTime().getTime();
                DatePickerDialog datePickerDialog= new DatePickerDialog(Cal_TIET.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {

                                    str = (dayOfMonth + "-" + (monthOfYear+ 1) + "-" + year);
                                    Toast.makeText(Cal_TIET.this,"End Date set",Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                    Toast.makeText(Cal_TIET.this,"Exception",Toast.LENGTH_SHORT).show();
                                }
                                starttextInputEditText.setText(str);

                            }
                        },myear,mmonth,mday);
                datePickerDialog.getDatePicker().setMaxDate(max);
                datePickerDialog.setTitle("");
                datePickerDialog.show();
            }
        });
        ImageView endimageView=new ImageView(this);
        endimageView.setImageDrawable(getResources().getDrawable(R.mipmap.cal));
        endimageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mmonth = c.get(Calendar.MONTH);
                mday = c.get(Calendar.DAY_OF_MONTH);
                myear = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog= new DatePickerDialog(Cal_TIET.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {

                                    str = (dayOfMonth + "-" + (monthOfYear+ 1) + "-" + year);
                                    Toast.makeText(Cal_TIET.this,"End Date set",Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e){
                                    Toast.makeText(Cal_TIET.this,"Exception",Toast.LENGTH_SHORT).show();
                                }
                                endtextInputEditText.setText(str);

                            }
                        },myear,mmonth,mday);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        //Layout params
        android.support.design.widget.TextInputLayout textInputLayout=new TextInputLayout(this);
        textInputLayout.setId(1);
        TextInputLayout.LayoutParams layoutParams1=new TextInputLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textInputLayout.addView(starttextInputEditText,layoutParams1);
        RelativeLayout.LayoutParams layoutParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin=50;
        RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.dyna_cal_layout);
        relativeLayout.addView(textInputLayout,layoutParams);

        //Start_Image
        RelativeLayout.LayoutParams layoutParams_startImage=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams_startImage.addRule(RelativeLayout.ALIGN_RIGHT,1);
        layoutParams_startImage.addRule(RelativeLayout.ALIGN_TOP,1);
        relativeLayout.addView(startimageView,layoutParams_startImage);

//end input text
        android.support.design.widget.TextInputLayout textInputLayoutend=new TextInputLayout(this);
        TextInputLayout.LayoutParams layoutParams2=new TextInputLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textInputLayoutend.addView(endtextInputEditText,layoutParams2);
        textInputLayoutend.setId(2);
        RelativeLayout.LayoutParams layoutParams3=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams3.topMargin=250;
        relativeLayout.addView(textInputLayoutend,layoutParams3);

        //end image
        RelativeLayout.LayoutParams layoutParams_endImage=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams_endImage.addRule(RelativeLayout.ALIGN_RIGHT,2);
        layoutParams_endImage.addRule(RelativeLayout.ALIGN_TOP,2);
        relativeLayout.addView(endimageView,layoutParams_endImage);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
