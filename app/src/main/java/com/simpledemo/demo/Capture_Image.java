package com.simpledemo.demo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Capture_Image extends AppCompatActivity {
    Button camera;
    ImageView imageSaved;
    int CAP=1;
//--------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture__image);
        imageSaved = (ImageView) findViewById(R.id.imageView2);

     //enabling Home button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        camera = (Button) findViewById(R.id.button);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(in, CAP);
            }
        });
    }
//--------------------------------------------------------------------------------------------
    //back to home arrow
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //Formatting date
    public String date(){
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyMMdd_HHmmss");
        String currentdate= simpleDateFormat.format(new Date());
        return currentdate;
    }

   //SaveImage
    protected void saveimage(Bitmap bitmap, String Currentdate){
        File file=new File(this.getCacheDir(),"photo_" + Currentdate + ".jpg");
        try{
            FileOutputStream fileOutputStream=new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        }
        catch(FileNotFoundException ex){
            Toast.makeText(getApplicationContext(),"error FILE...",Toast.LENGTH_SHORT);
        }
        catch(IOException i){
            Toast.makeText(getApplicationContext(),"error IO...",Toast.LENGTH_SHORT);
        }
    }

//Reading a bitmapfrom a string
    private Bitmap read(String filename){
        Bitmap bitmap=null;
        File file=new File(this.getCacheDir(),filename);
        try{
            FileInputStream fileInputStream=new FileInputStream(file);
            bitmap = BitmapFactory.decodeStream(fileInputStream);
        }
        catch(FileNotFoundException ex){
            Toast.makeText(getApplicationContext(),"error IO reading...",Toast.LENGTH_SHORT);
        }
        return bitmap;
    }

    //setting image on activityresult from current  click  and from filenmae
    public void onActivityResult(int requestCode, int resultCode, Intent Data) {
        super.onActivityResult(requestCode, resultCode, Data);
        if (requestCode == CAP) {
            Bitmap image = (Bitmap) Data.getExtras().get("data");
            ImageView img = (ImageView) findViewById(R.id.imageView);
            img.setImageBitmap(image);


            String Cdate=date();
            String storefilename="photo_" + Cdate + ".jpg";
            saveimage(image,Cdate);
            Bitmap bitmap=read(storefilename);
            imageSaved.setImageBitmap(bitmap);
        }
    }
}
