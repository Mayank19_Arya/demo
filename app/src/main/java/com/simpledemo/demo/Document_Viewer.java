package com.simpledemo.demo;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

public class Document_Viewer extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    private Button button;
    public ImageView imageView1, imageView2;
    private FileInputStream inputStream;
    private TextView textView;
    private EditText editText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document__viewer);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView3);
        button = (Button) findViewById(R.id.button1);
        textView = (TextView) findViewById(R.id.textView7);

        //adding  onclick listener to button
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent();
                i.addCategory(Intent.CATEGORY_OPENABLE);
                // i.setType("text/*");
                i.setType("*/*");
                String[] types = {"application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "image/*", "text/plain"};
                i.putExtra(Intent.EXTRA_MIME_TYPES, types);
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedUri = data.getData();
                String s = selectedUri.toString();
                String mpath = URI.create(s).getPath();
                // String path="storage/emulated/0/"+m.substring(18);
                String path = Environment.getExternalStorageDirectory() + "/" + mpath;
                File file = new File(path);
                long fileSize = file.length();

                //checking if file path exists
                if (file.exists()) {
                    System.out.println("exists");
                    System.out.println("size:" + fileSize);
                } else {
                    System.out.println("size:" + fileSize + path);
                    System.out.println(s);
                    System.out.println(mpath);
                    System.out.println("error");
                }
            }
        }
    }


}
