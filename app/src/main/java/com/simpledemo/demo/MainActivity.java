package com.simpledemo.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{
Button simpleCalander,TIETCalander,camera,dynamic_nav;
final String TAG="df";
    private String TAG_broadcast="tag";
    String msgfrom=null;
    String msgBody=null;
    EditText otp;
    boolean mIsReceiverRegistered = false;
    MyBroadcastReceiver mReceiver = null;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        //adding new button dynamically sample
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu menu=navigationView.getMenu();
        menu.add("new option");
        MenuItem item=menu.getItem(4);
        item.setIcon(R.mipmap.ic_launcher);
        item.setCheckable(true);
        item.setEnabled(true);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
                return false;
            }
        });
        //Buttons and their onclick listener
        TIETCalander = (Button) findViewById(R.id.tiet_cal);
        TIETCalander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), Cal_TIET.class);
                startActivity(i);
            }
        });
        simpleCalander = (Button) findViewById(R.id.scal);
        simpleCalander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), Cal_Simple.class);
                startActivity(i);
            }
        });
        camera=(Button)findViewById(R.id.button);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), Capture_Image.class);
                startActivity(i);
            }
        });
        dynamic_nav=(Button)findViewById(R.id.dyna_nav);
        dynamic_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), dynamic_nav.class);
                startActivity(i);
            }
        });
        //adding button dynamically
        Button dynamicButton=new Button(this);
        dynamicButton.setText("Dynamic Button");
        dynamicButton.setBackgroundColor(Color.parseColor("#15B0B4"));
        dynamicButton.setTextColor(Color.parseColor("#ffffff"));
        RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.content_main);
        RelativeLayout.LayoutParams layoutParams=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin=20;
        layoutParams.addRule(RelativeLayout.BELOW,R.id.dyna_nav);
        relativeLayout.addView(dynamicButton,layoutParams);
        otp=(EditText)findViewById(R.id.otp_field);


    }
    //getting a msg content on msg recieved
    public class Broadcast_Reciever  extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
                Log.v(TAG_broadcast,"msg recieved");
                Bundle bundle=intent.getExtras();
                SmsMessage msg[]=null;


                if(bundle != null){
                    Object[] pdus = (Object[])bundle.get("pdus");
                    msg = new SmsMessage[pdus.length];
                    for(int i=0; i<msg.length; i++){
                        msg[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        msgfrom = msg[i].getOriginatingAddress();

                        msgBody = msg[i].getMessageBody();
                    }
                    Toast.makeText(context, "msg from:"+msgfrom+" message :" + msgBody, Toast.LENGTH_SHORT).show();}
                Log.v(TAG_broadcast,"msgfrom:"+msgfrom+" Message:"+msgBody);


            }

        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calendar) {
            Intent i=new Intent(getApplicationContext(),Cal_Simple.class);
            startActivity(i);
        } else if (id == R.id.nav_newcal) {
            Intent i=new Intent(getApplicationContext(),Cal_TIET.class);
            startActivity(i);

        } else if (id == R.id.nav_camera) {
            Intent i=new Intent(getApplicationContext(),Capture_Image.class);
            startActivity(i);
        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //
    public void onRestart(){
        super.onRestart();
        if (!mIsReceiverRegistered) {
            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("YourIntentAction"));
            mIsReceiverRegistered = true;
        }
        Log.v(TAG,"MainActivity: Restart");
    }
   //update UI on msg recieved
    private void updateUI(Intent intent) {
        otp.setText(msgfrom);
    }
    private class MyBroadcastReceiver extends com.simpledemo.demo.Broadcast_Reciever{

        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    }
    public void onStart(){
        super.onStart();
        Log.v(TAG,"MainActivity :Start");
    }

    public void onPause(){
        super.onPause();
        if (mIsReceiverRegistered) {
            unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
        Log.v(TAG,"MainActivty:Pause");
    }
    public void onStop(){
        super.onStop();
        Log.v(TAG,"MainActivty:Stop");
    }
    public void onResume(){
        super.onResume();
        Log.v(TAG,"MainActivity: Resume");
    }
    public void onDestroy(){
        super.onDestroy();
        Log.v(TAG,"MainActivity: Destroy");
    }

}
