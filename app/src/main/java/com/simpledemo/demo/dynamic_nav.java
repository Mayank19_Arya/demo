package com.simpledemo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.view.menu.SubMenuBuilder;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class dynamic_nav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);  //initialising navigationview item
        Menu menu = navigationView.getMenu();             //initialising MEnu object
        MenuItem cal = menu.findItem(R.id.nav_calendar);       //initialising menu item
        cal.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return false;
            }
        });
        cal.setTitle("new Cal");
        cal.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return true;
            }
        });
        MenuItem unknown = menu.findItem(R.id.nav_manage);   //manpulating old menu
        unknown.setTitle("Changed");
        MenuItem camera = menu.findItem(R.id.nav_camera);
        camera.setTitle("new Camera");

        MenuItem calplus = menu.findItem(R.id.nav_newcal);
        calplus.setTitle("new Calendar+");
        menu.add("Dynamic Option");      //adding new option in navigation menu
        MenuItem new_menu_item = menu.getItem(4);
        new_menu_item.setIcon(R.mipmap.ic_launcher);   //adding icon to option
        new_menu_item.setCheckable(true);
        new_menu_item.setEnabled(true);
        new_menu_item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return false;
            }
        });
        //
        //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //  Menu menu=navigationView.getMenu();
        menu.add("item name");
        MenuItem item = menu.getItem(5);
        item.setIcon(R.mipmap.ic_launcher);
        item.setCheckable(true);
        item.setEnabled(true);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return false;
            }
        });
        //
        //additional <code>
    /*    menu.add("Dynamic Option");
        MenuItem additional_item=SubMenu.ge(1);
        additional_item.setIcon(R.mipmap.ic_launcher);
        additional_item.setCheckable(true);
        additional_item.setEnabled(true);
        additional_item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
                return false;
            }
        });*/
        SubMenu subMenuBuilder = menu.addSubMenu("");   //adding new items in sub menu
        subMenuBuilder.add("dynamic content");
        subMenuBuilder.add("additonal button");
        MenuItem additional_item = subMenuBuilder.getItem(1);
        additional_item.setIcon(R.mipmap.ic_launcher);
        additional_item.setCheckable(true);
        additional_item.setEnabled(true);
        additional_item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return false;
            }
        });


        navigationView.setNavigationItemSelectedListener(dynamic_nav.this);

        navigationView.setNavigationItemSelectedListener(this);

    }

    //home button method
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dynamic_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //onclick options for navigation view items whichare statically placed
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
